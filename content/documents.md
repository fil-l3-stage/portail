---
title: documents
---

# Documents concernant l'UE stage

- [Présentation de l'UE](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/UE_StageL3info.pdf)
  et [slides de l'amphi](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/StageSlide2022_2023L3.pdf).
- [Pré-convention](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/preconventionL3infoA.pdf)
  pour recueillir les informations avant établissement de la convention officielle.
- [Guide de rédaction](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/redactionRapStageL3Info.pdf)
  à respecter pour le rapport de stage.

# Fiches d'évaluation

- [fiche d'évaluation du stage](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/GrilleEvaluationL3infoTuteurEntreprise.pdf)
  ([english version](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/GrilleEvaluationTuteurEntreprise_english.pdf))
  à remplir par le tuteur entreprise.
- [fiche d'évaluation du rapport](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/GrilleEvaluationL3infoRapportTuteurUniv.pdf)
  à remplir par le tuteur universitaire.
- [fiche d'évaluation de la soutenance](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/GrilleEvaluationL3infoSoutenance.pdf)
  à remplir par les deux tuteurs entreprise et universitaire.

# Autres resources

- Présentation de la [démarche d'inscription à l'alternance](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/raw/public/resources/Logigramme_alternance.pdf)
- vos [droits en tant que stagiaires](https://www.etudiant.gouv.fr/cid124959/faq-|-stages-le-point-sur-vos-droits.html)
- [offres de stage en Europe](https://ec.europa.eu/eures/eures-apps/opp/page/main?lang=fr&app=0.17.1-build-2#/opportunity-search)
