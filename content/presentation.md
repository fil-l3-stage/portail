---
title: présentation
---
::: UE
Stage
:::
::: bcc
BCC 2 Développement personnel et professionnel
:::

Crédits : **3 ECTS**

Il n'y a pas de pré-requis.

# Organisation

Le stage est obligatoire et se déroule au S6 de la licence mention Informatique,
dans les parcours Info, Math-info, et MIAGE.

Le stage dure au minimum **12 semaines consécutives** et est **impérativement rémunéré** :
au 1er janvier 2024, la gratification minimum légale est de 4.35 € nets de l'heure.

L'étudiant(e) doit trouver lui même son stage.

# Dates

Recherche de stage

: dès octobre-novembre

Pré-convention et convention

: dès que vous avez trouvé votre stage et au plus tard début mars

Stage

: du **28 avril** au **18 juillet**, extensible jusqu'au 29 août inclus (rentrée des masters le 30 août)

Soutenances

: courant juin

# Responsables

- Julie Jacques
- Damien Pollet
- Flavien Stamper

Adresse mail à privilégier pour tous sujets relatifs à l'UE Stage : <SuiviStage-L3info@univ-lille.fr>
