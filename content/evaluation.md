---
title: évaluation
---

# Évaluation

La **note de stage** sur 20 est calculée comme suit :

<p class=".FORMULE">
15% Professionnalisme + 25% Entreprise + 60% Soutenance
</p>

à partir des éléments suivants :

- [Professionnalisme]{.NOTE} :
  démarche pendant la recherche de stage, respect des procédures et des échéances, proactivité et communication ;
- [Entreprise]{.NOTE} : évaluation du travail par le tuteur entreprise ;
- [Soutenance]{.NOTE} : évaluation de la soutenance par le tuteur universitaire et le tuteur entreprise.

# Soutenances

Toutes les soutenances ont lieu avant le jury, donc avant la fin du stage.
Les tuteurs universitaires coordonnent le choix du créneau de soutenance.

La fiche d'évaluation du stage remplie par le tuteur entreprise doit être envoyée au plus tard le jour de la soutenance (par mail à Flavien Stamper).

Les soutenances sont publiques, sauf demande de confidentialité de la part de l'entreprise.
Elles durent 35 minutes, en présence des deux tuteurs universitaire et entreprise :

- 20 minutes de présentation,
- 10 minutes de questions du public,
- 5 minutes de délibération du jury.

Le but de la soutenance est de présenter votre entreprise et votre travail, à un public qui ne le connaît pas ou pas en détail (par exemple un autre stagiaire passant sa soutenance au même créneau et ses tuteurs).

Votre objectif est de faire comprendre le contexte et l'importance de votre contribution, la charge de travail et les problèmes rencontrés.
Il est plus important de démontrer une prise de recul et de faire un bilan personnel sur votre expérience à mi-parcours du stage, que d'entrer dans les détails de la réalisation technique.

# Abstract / résumé en anglais

- Échéance : jeudi 25 mai à minuit
- Format : un document PDF de deux pages hors images ou graphiques, police taille 12.
- Évaluation : le résumé est évalué par votre enseignant d'anglais; la note contribue au BCC2 avec celle du stage

Présentation succinte du stage, en anglais. Contenu / suggestion de plan :

- présentation de l'entreprise
- avant le stage : vos préparations, difficultés envisagées, questions que vous vous êtes posées
- les attentes de l'entreprise
- les tâches prévues
- votre bilan intermédiaire (selon l'avancement au moment où vous rédigez)
