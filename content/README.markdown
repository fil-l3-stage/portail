# Stage L3 — contenu du portail FIL

**Attention, NE PAS ÉDITER manuellement:**
le contenu de cette branche `public` est généré automatiquement par l'intégration continue,
à partir de la [branche `main` de ce même dépôt](https://gitlab.univ-lille.fr/fil-l3-stage/portail "fil-l3-stage/portail (gitlab université de Lille)").

Vues publiques de ce contenu:
[L3 info](https://portail.fil.univ-lille.fr/ls6/stage) ou
[L3 miage](https://portail.fil.univ-lille.fr/ls6miage/stage).
