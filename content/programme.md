---
title: programme
---
[baip]: http://pass-pro.univ-lille1.fr/ "bureau d'aide à l'insertion professionnelle"
[moodle]: https://moodle.univ-lille.fr/course/view.php?id=8969

# Objectifs

Le stage est une étape importante dans le cursus de l'étudiant(e), qui lui permet :

- de découvrir le monde professionnel en informatique,
- de développer ses compétences techniques et ses connaissances professionnelles,
- de faire évoluer son projet professionnel.

Il est impératif de commencer les recherches dès le mois **de novembre.**

# La recherche de stage

Dès le début L'UE l'étudiant(e) doit :

- S'inscrire sur Moodle dans le cours [**[L3 Info][L3 MIAGE] Stage**][moodle] avec sa clé selon le groupe Info et Miage.
Il est important de respecter le mode d'inscription sinon vous ne pourrez accéder aux informations spécifique (i.e les rendus…), aux différents groupes.
- Consulter toutes les informations sur Moodle (Présentation des stages, Comment trouver un stage…)
- Déposer sur Moodle, dans la rubrique Suivi de Stage, son CV et l'ensemble des lettres de motivation transmise à chacune des entreprises contactées chaque mois, remplir la pré-convention etc.
- Consulter le [bureau d'aide à l'insertion professionnelle (BAIP Pass'Pro)][baip] qui propose des activités spécifiques pour aider les étudiant(e)s, faire un CV, faire une lettre de motivation, entretiens…

Les étudiant(e)s qui le demandent pourront être reçus individuellement pour être accompagnés dans leur recherche de stage soit par les encadrantes du module, soit par le personnel du service [BAIP Pass'Pro][baip] de l'Université.

# Quand l'étudiant(e) a trouvé un stage

La procédure à suivre est

1. Faire remplir **la pré-convention** par l'entreprise.
2. Déposer **la pré-convention** remplie sur Moodle, en y joignant les documents demandés.
3. Saisir les informations de la pré-convention sur [l'application de suivi][suivi].

**Attention :**

- La validation de la pré-convention se fait **uniquement** sur [l'application de suivi][suivi] par les responsables des stages.
- Pour qu'on puisse valider le sujet, il faut détailler les compétences informatiques requises et les activités confiées à l'étudiant ; n'oubliez pas non plus l'entreprise car c'est ce qui différencie les préconventions renseignées de celles qui sont encore vierges dans l'application.
- Pour le tuteur universitaire, indiquez le nom du responsable des stages (INFO: Julie Jacques — MIAGE: Damien Pollet). Le tuteur effectif vous sera attribué en cours de stage.

# Quand la pré-convention est validée par le(la) responsable des stages

- L'étudiant(e) remplit la convention de stage **définitive** sur l'application dédiée.
- Le secrétariat pédagogique vous convoque pour signer la convention, et prend en charge le reste du circuit de signatures :
étudiant(e), puis entreprise, responsable des stages, et finalement doyen/président de l'université.
- Le stage ne peut pas démarrer tant que la convention n'est pas signée, et le circuit de signatures nécessite du temps (plusieurs jours ouvrables).

Attention, sont transmises au **BAIP**, ce qui augmente les délais de signature :
- les conventions spécifiques demandées par certaines entreprises.
- les conventions et les différents éléments pour les stages à l'étrangers.
