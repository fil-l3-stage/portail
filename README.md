# Stage L3 — contenu du portail FIL

[![pipeline status](https://gitlab.univ-lille.fr/fil-l3-stage/portail/badges/main/pipeline.svg)](https://gitlab.univ-lille.fr/fil-l3-stage/portail/-/commits/main)

## Pour mettre à jour le contenu du portail

Il suffit d'éditer les fichiers sous `content/`, commit et push.
Il n'y a rien d'autre à faire : l'intégration continue de Gitlab intervient automatiquement après chaque push, convertit les pages rédigées en Markdown (fichiers `*.md`), et publie la nouvelle version.

Infos à mettre à jour chaque année, dans `content/presentation.md` :
- intervenants et groupes
- dates d'échéances

## Comment ça marche

La procédure de conversion et publication est dans le `Makefile`; `make help` en documente les cibles principales.
La publication consiste à committer le contenu du répertoire `output/` dans la branche dédiée `public` qui est disjointe de `main` (pas de commit ancêtre commun, hiérarchie différente).
Il n'y a aucune raison de travailler directement sur `public`.

Pour que ça marche avec l'intégration continue il faut :
- que la CI/CD travaille en `git clone` (Settings — CI/CD — General pipelines — Git strategy)
- créer un /project access token/ avec le rôle `Developer` et le scope `write_repository` (pour pouvoir `git push`)
- mettre ce token dans la variable `DEPLOY_TOKEN` de la CI/CD
Les tokens expirent donc la manip est à refaire tous les ans.

### Conversion en local

Utile pour par exemple contrôler le balisage HTML généré. Nécessite l'installation de [`pandoc`](https://pandoc.org).

Invoquez `make build`, ou juste `make`; le résultat est produit dans le répertoire `output/`.

### Publication _à la main_

**Attention :** toute version publiée manuellement, ou toute sera écrasée par l'intégration continue au commit+push suivant, donc au mieux c'est redondant.

Invoquez `make publish`; c'est ce que fait l'intégration continue (fichier `.gitlab-ci.yml`).

## Maintenance du portail

- [fonctionnement du portail](https://www.fil.univ-lille.fr/portail/index.php?dipl=PRIVE&label=Documents) et [contacts](https://www.fil.univ-lille.fr/portail/index.php?dipl=PRIVE&label=Gestion)
