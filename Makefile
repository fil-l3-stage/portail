# Root of the tree of source files.
# The build converts *.md only, any copies all other files verbatim.
SRC := content

# Publishable tree, generated from $(SRC).
# Have a look, but don't edit those.
OUT := output

# Sets of source files or build targets. Internal.
SRC_MARKUP := $(shell find $(SRC) -type f -name '*.md')
SRC_OTHERS := $(shell find $(SRC) -type f -not -name '*.md' -not -name '.*')
SRC_DIRS   := $(shell find $(SRC) -type d -mindepth 1)

OUT_MARKUP := $(SRC_MARKUP:$(SRC)/%.md=$(OUT)/%.php)
OUT_OTHERS := $(SRC_OTHERS:$(SRC)/%=$(OUT)/%)
OUT_DIRS   := $(SRC_DIRS:$(SRC)/%=$(OUT)/%)

.PHONY: build publish clean
.DEFAULT_GOAL := build

# Main entry point targets

build : $(OUT_MARKUP) $(OUT_OTHERS) ## Generate publishable files, converting *.md to *.php

publish : | $(OUT)/.git build ## Build then push to the 'public' branch if there are changes
	if [ -n "$$(git -C $(OUT) status --porcelain)" ]; then \
		cd $(OUT) ;\
		git add . ;\
		git commit --message 'Publish from $(shell git rev-parse HEAD)' ;\
		git push --push-option ci.skip origin 'HEAD:public' ;\
	fi

clean : ## Wipe all generated files
	rm -fr $(OUT)
	git worktree prune

# Ensure $(OUT) is setup for publishing, by making it a wortree for the 'public' branch.
# Under CI, commit as whoever caused the build, but authentify using a token (environment variable
# DEPLOY_TOKEN set from the gitlab side)
$(OUT)/.git :
	make clean
	git fetch origin 'public:public'
	git worktree add --no-checkout $(OUT) public
	if [ "$$CI" == 'true' -a -n "$$DEPLOY_TOKEN" ]; then \
		git config user.name  "$$GITLAB_USER_NAME" ;\
		git config user.email "$$GITLAB_USER_EMAIL" ;\
		git remote set-url origin "https://cicd:$${DEPLOY_TOKEN}@$${CI_SERVER_HOST}/$${CI_PROJECT_PATH}.git" ;\
	fi

$(OUT) :
	mkdir $(OUT)

$(OUT_DIRS) : $(OUT)
	@mkdir -p $@

.SECONDEXPANSION: # oooh, magic *_* (needed to ensure parent directories exist)

$(OUT)/% : $(SRC)/% | $$(@D)
	cp $< $@

$(OUT)/%.php : $(SRC)/%.md signature.html | $$(@D)
	pandoc --from markdown --to html --template signature --shift-heading-level-by=1 \
	       --output $@ $<

# Some shell magic to auto-document the main targets. To have a target appear in
# the output, add a short, one-line comment with a double ## on the same line as
# the target.
#
# See http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html

.PHONY: help

help: ## Describe the main targets (this list)
	@echo "Main targets you can build:"
	@awk -F ':|## *' \
		'/^[^\t].+:[^=].*##/ {\
			printf "  \033[36m%s\033[0m#%s\n", $$1, $$NF \
		}' $(MAKEFILE_LIST) \
	| column -s# -t
